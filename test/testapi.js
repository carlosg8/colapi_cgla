var mocha = require('mocha');
var chai =  require('chai');
var chaiHttp =  require('chai-http');
var server = require('../server_v3');

var should = chai.should();

chai.use(chaiHttp) // Configurar chai como mòdulo HTTP

describe('Pruebas Colapi', () => {
  it('Test BBVA', (done) => {
    chai.request('http://www.bbva.com')
    .get('/')
    .end((err, res) => {
      console.log(res)
      res.should.have.status(200)
      done();
    })
  });
  it('FIFA BBVA', (done) => {
    chai.request('http://www.bbva.com')
    .get('/')
    .end((err, res) => {
      console.log(res)
      res.should.have.status(200)
      done();
    })
  });
  it('Mi API funciona', (done) => {
    chai.request('http://localhost:3000')
    .get('/Colapi/v3/users/102')
    .end((err, res) => {
      console.log(res)
      res.should.have.status(200)
      done();
    })
  });
  it('Devuelve un array de usuarios', (done) => {
    chai.request('http://localhost:3000')
    .get('/Colapi/v3/users/')
    .end((err, res) => {
      console.log(res.body)
      res.body.should.be.a('array')
      done();
    })
  });
  it('Devuelve al menos un elemento', (done) => {
    chai.request('http://localhost:3000')
    .get('/Colapi/v3/users/')
    .end((err, res) => {
      console.log(res.body)
      res.body.length.should.be.gte(1)
      done();
    })
  });
  it('validar primer elemento', (done) => {
    chai.request('http://localhost:3000')
    .get('/Colapi/v3/users/')
    .end((err, res) => {
      console.log(res.body[0])
      res.body[0].should.have.property('first_name')
      done();
    })
  });
  it('Validar crea elemento', (done) => {
    chai.request('http://localhost:3000')
    .post('/Colapi/v3/users/')
    .send()
    .end((err, res, body) => {
      console.log(res.body[0])
      body.should.be.eql('first_name')
      done();
    })
  });
});
