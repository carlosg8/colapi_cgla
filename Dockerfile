# Imagen inicial a partir de la cual creamos nuestra imagen
FROM node

# Definimos directorio del contenedor
WORKDIR /colapi_cgla

# Añadimos contenido del proyecto en directorio del contenedor.
ADD . /colapi_cgla

# Puerto va a escuchar el contenedor
EXPOSE 3000

# Comandos para lanzar la API REST 'colapi'
#CMD ["npm","start"]
CMD ["node","server_v3.js"]
