var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var port = process.env.PORT || 3000;
var usersFile = require('./users.json');
var URLbase = '/Colapi/v1/'

app.listen(port);
app.use(bodyParser.json());
console.log("Colapi escuchando en puerto " + port + "....");

app.get('/Colapi/v1',
  function (req, res) {
    console.log("GET /Colapi/v1");
    console.log(req.params);
    console.log(req.headers);
    res.send({"msg":"Peticion correcta"});
  }
);

app.post('/Colapi/v1',
  function (req, res) {
    res.send("Respuesta POST request");
  }
);

app.put('/Colapi/v1',
  function (req, res) {
    res.send('Respuesta PUT request at /user');
  }
);

app.delete('/Colapi/v1',
  function (req, res) {
    res.send('Respuesta DELETE request at /user');
  }
);

/*app.get('/Colapi/v1/users',
  function (req, res) {
    console.log("GET /Colapi/v1/users");
    res.sendfile('./users.json');
  }
);*/
//Se envia paramatro
app.get(URLbase + '/users/:id',
  function (req, res) {
    console.log("GET /Colapi/v2/users/:id");
    var pos = req.params.id;
    res.send(usersFile[pos-1]);
  }
);
//Se envia dos paramatros
/*app.get(URLbase + 'users/:id/:otro',
  function (req, res) {
    console.log("GET /Colapi/v1/users/:id");
    console.log('req.params.id: ' + req.params.otro)
    var pos = req.params.id;
    res.send(usersFile[pos-1]);
  }
);*/

//Peticiòn GET con parametros
app.get(URLbase + 'users/:id/:otro',
  function (req, res) {
    console.log("GET /Colapi/v1/users/:id");
    console.log('req.params.id: ' + req.params.otro)
    var pos = req.params.id;
    var respuesta = req.params.id + ', ' + req.params.otro;
    res.send(respuesta);
  }
);

//Peticion GET con Query String(req.query)
app.get(URLbase + 'users',
  function(peticion,respuesta){
    console.log('GET con query string');
    console.log(peticion.query.id);
    console.log(peticion.query.country);
    respuesta.send({"msg":"GET con query string"});
  }
);

//Enviar datos a travez del body
//Peticion POST devuelve mensaje
app.post(URLbase + 'users',
  function (req, res) {
    console.log("Post correcto!");
    console.log(req.body);
    console.log(req.body.first_name);
    var jsonId = {};
    var newID = usersFile.length + 1;
    jsonId = req.body;
    jsonId.id = newID;
    usersFile.push(jsonId);
    console.log(usersFile);
    res.send({"msg":"Usuario creado correctamente.", "id": jsonId})
    //res.sendFile('users.json', {root: __dirname});
  }
);

//Peticion PUT
app.put(URLbase + 'users/:id',
  function(req, res){
    console.log("PUT /colapi/v2/users/:id");
    var idBuscar = req.params.id;
    var updateUser = req.body;
    for(i = 0; i < usersFile.length; i++) {
      console.log(usersFile[i].id);
      if(usersFile[i].id == idBuscar) {
        res.send({"msg" : "Usuario actualizado correctamente.", updateUser});
      }
    }
    res.send({"msg" : "Usuario no encontrado.", updateUser});
  });

//Peticion DELETE
app.put(URLbase + 'users_del/:id',
  function(req, res){
    console.log("DELETE /colapi/v2/users/:id");
    var idBuscar = req.params.id;
    for(i = 0; i < usersFile.length; i++) {
      console.log(usersFile[i].id);
      if(usersFile[i].id == idBuscar) {
        usersFile.splice(idBuscar-1 ,1);
        res.send({"msg" : "Usuario actualizado correctamente.", updateUser});
      }
    }
    res.send({"msg" : "Usuario no encontrado.", updateUser});
  });
