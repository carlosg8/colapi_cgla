var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var requestJson = require('request-json');
var port = process.env.PORT || 3000;
var usersFile = require('./users.json');
var URLbase = '/Colapi/v3/';
var basemLabURL = 'https://api.mlab.com/api/1/databases/colapidb_cgla/collections/';
var apiKey = 'apiKey=acYLyXRecvHE6M58s4gKwvi6cPNk8f_P';

app.listen(port);
app.use(bodyParser.json());
console.log("Colapi escuchando en puerto " + port + "....");

//Peticion GET users
app.get(URLbase + 'users/',
  function (req, res) {
    console.log("GET /Colapi/v3/users/");
    console.log(req.params.id);
    var id = req.params.id;
//    var queryString = 'q={"id":' + id + '}&';
    var queryString2 = 'f={"_id":0}&';
    var httpClient = requestJson.createClient(basemLabURL);
    httpClient.get('user?' +  queryString2 + apiKey,
      function(err,respuestaMLab,body){
          console.log("Respuesta mLab users correcta.");
          var respuesta = body;
          res.send(respuesta);
      }
    );
  }
);

//Peticion GET con id en mLab
app.get(URLbase + 'users/:id',
  function (req, res) {
    console.log("GET /Colapi/v3/users/:id");
    console.log(req.params.id);
    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';
    var queryString2 = 'f={"_id":0}&';
    var httpClient = requestJson.createClient(basemLabURL);
    httpClient.get('user?' + queryString + queryString2 + apiKey,
      function(err,respuestaMLab,body){
          console.log("Respuesta mLab correcta.");
          var respuesta = body[0];
          res.send(respuesta);
      }
    );
  }
);

//Peticion GET account en mLab
app.get(URLbase + 'account',
  function (req, res) {
    console.log("GET /Colapi/v3/account");
    var queryString = 'f={"_id":0}&';
    var httpClient = requestJson.createClient(basemLabURL);
    httpClient.get('account?' + queryString + apiKey,
      function(err,respuestaMLab,body){
          console.log("Respuesta mLab account correcta.");
          var respuesta = body;
          res.send(respuesta);
      }
    );
  }
);

//Peticion GET de las cuentas de un usuario con id en mLab
app.get(URLbase + 'users/:id/account',
  function (req, res) {
    console.log("GET /Colapi/v3/users/:id/account");
    console.log(req.params.id);
    var id = req.params.id;
    var queryString = 'q={"userId":' + id + '}&';
    var queryString2 = 'f={"_id":0}&';
    var httpClient = requestJson.createClient(basemLabURL);
    httpClient.get('account?' + queryString + queryString2 + apiKey,
      function(err,respuestaMLab,body){
          console.log("Respuesta mLab account correcta.");
          var respuesta = body;
          res.send(respuesta);
      }
    );
  }
);

//Peticion GET movement en mLab
app.get(URLbase + 'users/:id/account/:IBAN/movement/',
  function (req, res) {
    console.log("GET /Colapi/v3/users/:id/account/:IBAN/movement/");
    console.log(req.params.IBAN);

    var cuenta = req.params.IBAN
    var queryString = 'q={"IBAN":' + cuenta + '}&';
    var queryString2 = 'f={"_id":0}&';
    var httpClient = requestJson.createClient(basemLabURL);
    httpClient.get('movement?' + queryString + queryString2 + apiKey,
      function(err,respuestaMLab,body){
          console.log("Respuesta mLab movement correcta.");
//          var respuesta = {};
//          respuesta = !err?body:{"msg": "Error al recuperar movement mLab"};
          var respuesta = body;
          res.send(respuesta);
      }
    );
  }
);

//POST para crear un Usuario
app.post(URLbase + 'users',
  function(req, res) {
    var clienteMlab = requestJson.createClient(basemLabURL + "user?" + apiKey)
    clienteMlab.post('', req.body,
      function(err, resM, body) {
        console.log("Usuario creado correctamente");
        console.log(body);
        res.send(body)
      }
    );
  }
);

//PUT para modificar un Usuario
app.put(URLbase + 'users/:id',
  function(req, res) {
    clienteMlab = requestJson.createClient(basemLabURL + "user")
    var cambio = '{"$set":' + JSON.stringify(req.body) + '}'
    clienteMlab.put('?q={"id": ' + req.params.id + '}&' + apiKey, JSON.parse(cambio),
      function(err, resM, body) {
        console.log("Usuario modificado.");
        console.log(body);
        res.send(body)
      }
    );
  }
);

//Login
app.post(URLbase + 'login',
  function(req, res) {
    var usuario = req.body.email;
    var passwd = req.body.password;
//    var queryString = 'q={"email":"' + usuario + '"}&q={"password":"' + passwd + '"}&';
    var queryString = 'q={"email":"' + usuario + '"}&';
    var clienteMlab = requestJson.createClient(basemLabURL);

    clienteMlab.get("user?" + queryString + apiKey,
      function(err, resM, body) {
        var id = body[0].id;
        if(body[0].password == passwd){
          var cambio = '{"$set":{"logged":true}}';
          clienteMlab.put('user?q={"id":'+body[0].id+'}&'+apiKey, JSON.parse(cambio),
            function(error,resP,body){
              var respuesta = {};
              respuesta = !error ? {"msg":"Usuario Logueado correctamente.","id":id} : {"msg":"Usuario No Logueado."};
              res.send(respuesta);
            }
          );
        } else {
            res.send({"msg":"Usuario y password no encontrado."})
        }
      }
    );
  }
);

//Logout
app.post(URLbase + 'logout',
  function(req, res) {
    var usuario = req.body.email;
//    var passwd = req.body.password;
//    var queryString = 'q={"email":"' + usuario + '"}&q={"password":"' + passwd + '"}&';
    var queryString = 'q={"email":"' + usuario + '"}&';
    var clienteMlab = requestJson.createClient(basemLabURL);

    clienteMlab.get("user?" + queryString + apiKey,
      function(err, resM, body) {
        if(body[0].email == usuario){
          if(body[0].logged == true){
            var cambio = '{"$unset":{"logged":true}}';
            clienteMlab.put('user?q={"id":'+body[0].id+'}&'+apiKey, JSON.parse(cambio),
              function(error,resP,body){
                var respuesta = {};
                respuesta = !error ? {"msg":"Ha salido satisfactoriamente."} : {"msg":"Error en consulta."};
                res.send(respuesta);
              }
            );
          } else {
            res.send({"msg":"El usuario no ha sido loguado."});
          }
        } else {
            res.send({"msg":"Usuario no encontrado."});
        }
      }
    );
  }
);
